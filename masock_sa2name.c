/** \file masock_sa2name.c
 *  (C) Copyright 2001,2013 by Matthias Andree
 *  \author Matthias Andree
 *  \year 2001,2013
 */

#include "config.h"

#undef _GNU_SOURCE
#define _GNU_SOURCE	// to expose nonstandard members in in.h structures to
			// fix compilation in strict conformance mode on Linux

#include "masock.h"
#include "critmem.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#ifndef __LCLINT__
#include <arpa/inet.h>
#endif /* not __LCLINT__ */
#include <netdb.h>

#include <string.h>

#ifdef WITH_DMALLOC
#include <dmalloc.h>
#endif

#include <errno.h>

/** Look up the host name belonging to the socket address. If compiled
 * with IPv6 support, use IPv6 lookup unless the address is actually an
 * IPv6-mapped IPv4 address.
 *
 * \return strdup'ed string containing the host name or NULL in case of
 * trouble, in which case the error code will be placed into the int 
 * variable pointed to by gai_error.
 */
char *
masock_sa2name(const struct sockaddr *sa /** socket address to convert */,
               const socklen_t addrlen   /** length of address in sa */,
               int *gai_error            /** @out variable to place error code into */)
{
    char host[NI_MAXHOST];
    *gai_error = getnameinfo(sa, addrlen, host, sizeof host, NULL, 0, NI_NAMEREQD);
    return (0 == *gai_error) ? critstrdup(host, "masock_sa2name") : 0;
}
