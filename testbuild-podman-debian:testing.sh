podman run --rm -it debian:testing sh -c '
	apt update -y && 
	apt upgrade -y &&
	apt install --no-install-recommends build-essential libpcre3-dev automake git ca-certificates -y &&
	git clone https://gitlab.com/leafnode-2/leafnode-2.git &&
	cd leafnode-2 &&
	autoreconf -svif &&
	./configure -C &&
	make -sj20 check'
