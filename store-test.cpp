#if __cpp_lib_format + 0 < 201907L
#pragma warning "This compilation unit requires C++20 formatting support"
#endif

#include <iostream>
#include <format>
#include <iterator>
#include <string>
#include <vector>
#include <cstdlib>
#include <cstdio>

#include "store.c"

int main(int argc, char **argv)
{
	using namespace std::string_literals;

	if (argc < 2) {
		std::cerr << std::format("Error: missing command line argument.\n"
					 "Usage: {} filename [...]\n", argv[0]);
		return EXIT_FAILURE;
	}

	const auto args = std::vector<std::string>(argv + 1, argv + argc);

	const auto linebuf = mastr_new(4095);

	for (const auto filename : args) {
		FILE *infile = fopen(filename.c_str(), "r");
		if (!infile) {
			std::cerr << std::format("Error: cannot open file {}: {} (errno={})\n", filename, strerror(errno), errno);
			return EXIT_FAILURE;
		}

		std::cout << std::format("### Reading {}:\n", filename);

		while (mastr_getfoldedln(linebuf, infile, -1) > 0) {
			mastr_chop(linebuf);
			std::cout << ">>>"s + mastr_str(linebuf) + "<<<\n";
		}

		if (ferror(infile) | /* <- bit-wise OR to avoid short-circuit! */ fclose(infile)) {
			std::cerr << std::format("Error reading file {}: {} (errno={})\n", filename, strerror(errno), errno);
		}
	}
					
	return EXIT_SUCCESS;
}
